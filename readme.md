# Coding Question

### Objective

Implement the following two programs, `tile` and `elit`:

+ `tile` reads 32 bytes and outputs a tile.
+ `elit` reads a tile and outputs 32 bytes.

Upon implementation the following commands will succeed:
```
$ cat 32.bin | tile | diff - 10x10.tile
$ cat 10x10.tile | elit | cmp - 32.bin
```

Create a private fork of this repo, and then (regularly) commit your progress while you develop a solution. When you are done, invite @ninebyte_admin (GitLab user) to your fork.

### Information

Program `tile`:

* Read 32 bytes from stdin as the DATA.  Take the SHA-256 of the DATA as the
  HASH.  Take the most significant 4 bytes of the HASH as the CHECKSUM.  Convert
  the DATA and CHECKSUM to hexadecimal (hex) representations.  Arrange the
  DATA hex into an 8x8 tile.  Place the 8x8 tile in the centre of a 10x10 tile.
  Split the CHECKSUM hex into 4 and arrange at {N,E,S,W} around the 8x8 tile.
  Fill the remainder of the tile with MIDDLE DOT (U+00B7).  The 10x10 tile is the
  TILE.  Write the TILE to stdout.

Program `elit`:

* Read 10 lines of 10 characters from stdout as the TILE.  Extract the inner 8x8
  tile as a hexadecimal (hex) representation of the DATA.  Extract from {N,E,S,W}
  hex bytes and combine into the CHECKSUM of 4 bytes.  Verify the CHECKSUM is the
  first 4 bytes of the SHA-256 of the DATA.  Confirm the MIDDLE DOTS are present.
  Write the DATA to stdout as binary.

```
$ cat 32.bin | xxd -p -c 64
a2c765b8ff9b592737427ba7efa643cff4c52348dcab5340395a0feaf3b8a36a
$ cat 32.bin | shasum -a 256 | cut -c1-8
2a0cc2c5
$ cat 10x10.tile
····2a····
·a2c765b8·
·ff9b5927·
·37427ba7·
cefa643cf0
5f4c52348c
·dcab5340·
·395a0fea·
·f3b8a36a·
····c2····
```

#

### Marks

The code should be of _quality_ - _correct_, _elegant_, _robust_ and _succinct_.

The code may follow any style you are willing to back as the author.
